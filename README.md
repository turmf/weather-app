## Display the current time and weather in OBS Studio!

![Screenshot](https://bitbucket.org/turmf/weather-app/raw/f6438389269cffa261f93bb2c3a5120d155b91e7/screenshot.png)

1. Get your own free [OpenWeather API key](https://openweathermap.org/api)
2. Change the configuration in the file `app.js`
3. Add a Browser source in OBS Studio
4. Change the URL to `file:///path/to/index.html`
5. Change the resolution to 700x200

---

## Add your own animated weather icons (optional)

Rendering SVG animations in real time is an unneccessary load on your CPU and can cause dropped frames depending on your system. I recommend converting animated SVG files to APNG.

The following steps were tested with Ubuntu 20.04. If you only have Windows you can install Windows Subsystem for Linux (untested) or a Virtual Machine with Ubuntu 20.04.

1. Install timecut and apngasm  
   Open a terminal  
   `sudo apt install npm apngasm`  
   `npm install timecut`
2. Use timecut to generate individual PNG files for each frame in your animation  
   `./node_modules/timecut/cli.js -R 30 -d 20 --transparent-background --keep-frames -V 64,64,deviceScaleFactor=4 animation.svg`  
   (you can modify the framerate, duration, resolution and filename)
3. Open the PNG files in an image viewer and make the loop look good  
   The files are in a folder called timecut-...  
   Find a "last" image that looks similar to the first image of your loop and delete all files after the last one
4. Assemble the APNG animation  
   `cd timecut-...` (you can use the TAB key to autocomplete)  
   `apngasm ../animation.apng image-0*.png 1 30` (30 is the framerate)  
   `cd ..`
5. Repeat steps 2-4 for the remaining icons
6. Place the .apng files in the icons folder
7. Change the icon names in `app.js`
