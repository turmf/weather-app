// OpenWeatherMaps configuration
const apikey = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
const latitude = 50.0;
const longitude = 10.0;
const weatherUpdateInterval = 300000;

// Local data configuration
const localDataEnable = false;
const localDataUrl = `http://localhost/sensors`; // example JSON data: {"temperature": "4.99", "humidity": "60.4", "timestamp": "806307009"}
const localDataTimeout = 120000;
const localDataUpdateInterval = 30000;


const iconElement = document.querySelector(".weather-icon");
const tempElement = document.querySelector(".temperature p");
const humElement = document.querySelector(".humidity p");
const timeElement = document.querySelector(".time p");

const weather = {};
weather.lastLocalData = 0;

const weatherCondIconTable = {
 200: 'thunder',
 201: 'thunder',
 202: 'thunder',
 210: 'thunder',
 211: 'thunder',
 212: 'thunder',
 221: 'thunder',
 230: 'thunder',
 231: 'thunder',
 232: 'thunder',
 300: 'rainy-4',
 301: 'rainy-5',
 301: 'rainy-6',
 310: 'rainy-4',
 311: 'rainy-5',
 312: 'rainy-6',
 313: 'rainy-5',
 314: 'rainy-6',
 321: 'rainy-5',
 500: 'rainy-2',
 501: 'rainy-3',
 502: 'rainy-5',
 503: 'rainy-6',
 504: 'rainy-6',
 511: 'rainy-7',
 520: 'rainy-4',
 521: 'rainy-5',
 522: 'rainy-6',
 531: 'rainy-3',
 600: 'snowy-3',
 601: 'snowy-5',
 602: 'snowy-6',
 611: 'rainy-7',
 612: 'snowy-4',
 613: 'snowy-5',
 615: 'snowy-6',
 616: 'snowy-5',
 620: 'snowy-4',
 621: 'snowy-5',
 622: 'snowy-6',
 701: 'cloudy',
 711: 'cloudy',
 721: 'cloudy',
 731: 'cloudy',
 741: 'cloudy',
 751: 'cloudy',
 761: 'cloudy',
 762: 'cloudy',
 771: 'cloudy',
 781: 'cloudy',
 800: 'day',
 801: 'cloudy-day-1',
 802: 'cloudy-day-2',
 803: 'cloudy-day-3',
 804: 'cloudy'
}

const weatherCondIconTableNight = {
 200: 'thunder',
 201: 'thunder',
 202: 'thunder',
 210: 'thunder',
 211: 'thunder',
 212: 'thunder',
 221: 'thunder',
 230: 'thunder',
 231: 'thunder',
 232: 'thunder',
 300: 'rainy-4',
 301: 'rainy-5',
 301: 'rainy-6',
 310: 'rainy-4',
 311: 'rainy-5',
 312: 'rainy-6',
 313: 'rainy-5',
 314: 'rainy-6',
 321: 'rainy-5',
 500: 'rainy-4',
 501: 'rainy-5',
 502: 'rainy-5',
 503: 'rainy-6',
 504: 'rainy-6',
 511: 'rainy-7',
 520: 'rainy-4',
 521: 'rainy-5',
 522: 'rainy-6',
 531: 'rainy-5',
 600: 'snowy-4',
 601: 'snowy-5',
 602: 'snowy-6',
 611: 'rainy-7',
 612: 'snowy-4',
 613: 'snowy-5',
 615: 'snowy-6',
 616: 'snowy-5',
 620: 'snowy-4',
 621: 'snowy-5',
 622: 'snowy-6',
 701: 'cloudy',
 711: 'cloudy',
 721: 'cloudy',
 731: 'cloudy',
 741: 'cloudy',
 751: 'cloudy',
 761: 'cloudy',
 762: 'cloudy',
 771: 'cloudy',
 781: 'cloudy',
 800: 'night',
 801: 'cloudy-night-1',
 802: 'cloudy-night-2',
 803: 'cloudy-night-3',
 804: 'cloudy'
}

/*
const weatherIconTable = {
 '01d': 'day',
 '01n': 'night',
 '02d': 'cloudy-day-1',
 '02n': 'cloudy-night-1',
 '03d': 'cloudy-day-3',
 '03n': 'cloudy-night-3',
 '04d': 'cloudy',
 '04n': 'cloudy',
 '09d': 'rainy-5',
 '09n': 'rainy-5',
 '10d': 'rainy-3',
 '10n': 'rainy-5',
 '11d': 'thunder',
 '11n': 'thunder',
 '13d': 'snowy-5',
 '13n': 'snowy-5',
 '50d': 'cloudy',
 '50n': 'cloudy',
}
*/

updateTime();
updateWeather();
updateLocalData();

function updateTime() {
    let t = setTimeout(function() { updateTime() }, 500);
    let date = new Date();
    timeElement.innerHTML = date.getFullYear() + "-" + ("0" + (date.getMonth() + 1)).substr(-2) + "-" + ("0" + (date.getDate())).substr(-2) + "<br>" + ("0" + date.getHours()).substr(-2) + ":" + ("0" + date.getMinutes()).substr(-2) + ":" + ("0" + date.getSeconds()).substr(-2);
}

function updateWeather() {
    let w = setTimeout(function() { updateWeather() }, weatherUpdateInterval);
    let api = `https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=${apikey}`;
    
    fetch(api)
        .then(function(response) {
            let data = response.json();
            return data;
        })
        .then(function(data) {
            weather.temperature = Math.round(data.main.temp - 273.15);
            weather.humidity = Math.round(data.main.humidity);

            let day = (data.weather[0].icon.charAt(2) == 'd');
            if (day) {
                weather.icon = weatherCondIconTable[data.weather[0].id];
            } else {
                weather.icon = weatherCondIconTableNight[data.weather[0].id];
            }
            
            //weather.icon = weatherIconTable[data.weather[0].icon];
        })
        .then(function() {
            iconElement.innerHTML = `<img src="icons/${weather.icon}.apng"/>`;
            if (localDataEnable) {
                let d = new Date();
                if (weather.lastLocalData + localDataTimeout < d.getTime()) {
                    tempElement.innerHTML = `${weather.temperature}<span>°C*</span>`;
                    humElement.innerHTML = `${weather.humidity}<span>%*</span>`;
                }
            } else {
                tempElement.innerHTML = `${weather.temperature}<span>°C</span>`;
                humElement.innerHTML = `${weather.humidity}<span>%</span>`;
            }
        });
}

function updateLocalData() {
    if (!localDataEnable) return;
    let l = setTimeout(function() { updateLocalData() }, localDataUpdateInterval);
    
    fetch(localDataUrl)
        .then(function(response) {
            let data = response.json();
            return data;
        })
        .then(function(data) {
            if (weather.localTimestamp != data.timestamp) {
                weather.localTemperature = Math.round(data.temperature);
                weather.localHumidity = Math.round(data.humidity);
                weather.localTimestamp = data.timestamp;
                let d = new Date();
                weather.lastLocalData = d.getTime();
            }
        })
        .then(function() {
            tempElement.innerHTML = `${weather.localTemperature}<span>°C</span>`;
            humElement.innerHTML = `${weather.localHumidity}<span>%</span>`;
        })
}
